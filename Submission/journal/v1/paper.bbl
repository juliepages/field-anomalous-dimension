
\providecommand{\href}[2]{#2}\begingroup\raggedright\begin{thebibliography}{10}

\bibitem{Chisholm:1961tha}
J.~S.~R. Chisholm, \emph{{Change of variables in quantum field theories}},
  \href{https://doi.org/10.1016/0029-5582(61)90106-7}{\emph{Nucl. Phys.}
  {\bfseries 26} (1961) 469}.

\bibitem{Politzer:1980me}
H.~D. Politzer, \emph{{Power Corrections at Short Distances}},
  \href{https://doi.org/10.1016/0550-3213(80)90172-8}{\emph{Nucl. Phys. B}
  {\bfseries 172} (1980) 349}.

\bibitem{Arzt:1993gz}
C.~Arzt, \emph{{Reduced effective Lagrangians}},
  \href{https://doi.org/10.1016/0370-2693(94)01419-D}{\emph{Phys. Lett. B}
  {\bfseries 342} (1995) 189}
  [\href{https://arxiv.org/abs/hep-ph/9304230}{{\ttfamily hep-ph/9304230}}].

\bibitem{Manohar:2018aog}
A.~V. Manohar, \emph{{Introduction to Effective Field Theories}},
  \href{https://arxiv.org/abs/1804.05863}{{\ttfamily 1804.05863}}.

\bibitem{Jenkins:2023bls}
E.~E. Jenkins, A.~V. Manohar, L.~Naterop and J.~Pag\`es, \emph{{Two Loop
  Renormalization of Scalar Theories using a Geometric Approach}},
  \href{https://arxiv.org/abs/2310.19883}{{\ttfamily 2310.19883}}.

\bibitem{Bednyakov:2014pia}
A.~V. Bednyakov, A.~F. Pikelner and V.~N. Velizhanin, \emph{{Three-loop SM
  beta-functions for matrix Yukawa couplings}},
  \href{https://doi.org/10.1016/j.physletb.2014.08.049}{\emph{Phys. Lett. B}
  {\bfseries 737} (2014) 129}
  [\href{https://arxiv.org/abs/1406.7171}{{\ttfamily 1406.7171}}].

\bibitem{Herren:2017uxn}
F.~Herren, L.~Mihaila and M.~Steinhauser, \emph{{Gauge and Yukawa coupling beta
  functions of two-Higgs-doublet models to three-loop order}},
  \href{https://doi.org/10.1103/PhysRevD.97.015016}{\emph{Phys. Rev. D}
  {\bfseries 97} (2018) 015016}
  [\href{https://arxiv.org/abs/1712.06614}{{\ttfamily 1712.06614}}], [Erratum:
  Phys.Rev.D 101, 079903 (2020)].

\bibitem{Herren:2021yur}
F.~Herren and A.~E. Thomsen, \emph{{On ambiguities and divergences in
  perturbative renormalization group functions}},
  \href{https://doi.org/10.1007/JHEP06(2021)116}{\emph{JHEP} {\bfseries 06}
  (2021) 116} [\href{https://arxiv.org/abs/2104.07037}{{\ttfamily
  2104.07037}}].

\bibitem{tHooft:1973mfk}
G.~'t~Hooft, \emph{{Dimensional regularization and the renormalization group}},
  \href{https://doi.org/10.1016/0550-3213(73)90376-3}{\emph{Nucl. Phys. B}
  {\bfseries 61} (1973) 455}.

\bibitem{Jenkins:2023rtg}
E.~E. Jenkins, A.~V. Manohar, L.~Naterop and J.~Pag\`es, \emph{{An Algebraic
  Formula for Two Loop Renormalization of Scalar Quantum Field Theory}},
  \href{https://arxiv.org/abs/2308.06315}{{\ttfamily 2308.06315}}.

\bibitem{Herzog:2017bjx}
F.~Herzog and B.~Ruijl, \emph{{The R$^{*}$-operation for Feynman graphs with
  generic numerators}},
  \href{https://doi.org/10.1007/JHEP05(2017)037}{\emph{JHEP} {\bfseries 05}
  (2017) 037} [\href{https://arxiv.org/abs/1703.03776}{{\ttfamily
  1703.03776}}].

\bibitem{Cao:2021cdt}
W.~Cao, F.~Herzog, T.~Melia and J.~R. Nepveu, \emph{{Renormalization and
  non-renormalization of scalar EFTs at higher orders}},
  \href{https://doi.org/10.1007/JHEP09(2021)014}{\emph{JHEP} {\bfseries 09}
  (2021) 014} [\href{https://arxiv.org/abs/2105.12742}{{\ttfamily
  2105.12742}}].

\end{thebibliography}\endgroup

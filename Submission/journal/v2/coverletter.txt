Dear Editor,

We thank the referee for the constructive comments. Below, we address
the referee’s points sequentially, and detail the changes we have made in the draft to address their
feedback. For convenience we list the questions and respective answers below. We additionally explain a couple of
further minor changes we made to the manuscript.

Best regards,
Aneesh Manohar, Julie Pages, Jasper Roosmale Nepveu

-----------------------------------------------------------------

"I believe the division into sections is a bit unusual (perhaps due to there
being just two). A good solution might be to rename the second section to
something more specific than “Example.” Although the issue is explained
with a concrete example, there are many (most) general observations in this
section."

- We have renamed the section "Example" into "Explicit Demonstration in the O(N) EFT". 
We have also started a new section called "General Remarks" towards the end of the paper. 

-----------------------------------------------------------------

"The authors might also want to consider a reference to https://inspirehep.
net/literature/1704723 along with references [1-4]"

- We have included the suggested reference.

-----------------------------------------------------------------

"On the first line of p. 3, the authors write that the beta functions for
the C couplings are ensured to be finite by the finiteness of the S-matrix;
however, as pointed out in refs.[6-8], there are examples of divergent beta
functions even in renormalizable theories, such as the SM (which certainly
has finite S-matrix elements). This would seem to contradict the author’s
claims. I suspect that the contradiction doesn’t change the wider conclusions
presented in the paper, but it should be clarified in a revision."

- We have adapted the initial statement from 

"Since the S-matrix is invariant under field redefinitions, and remains finite, this
means that any resulting [beta function] is finite." 

to 

"Since the S-matrix is invariant under field redefinitions, and remains finite, this means
that any resulting [beta function] is finite if the C_i are physical parameters. (We comment on the
case in which the C_i are unphysical parameters below.)"

and we have expanded the penultimate paragraph of the introduction, "Infinite $\beta$-functions ... in this paper",
into two paragraphs that explain the divergent beta functions of refs. [6-8] in more detail: "Finally, we remark... d,s,b, etc."

-----------------------------------------------------------------

"Below Eq. (2.21), the authors say that the field redefinition (2.20) removes all 
D2 and D4 dependence from the Lagrangian. Presumably, this is
only the case for the scalar kinetic term. If so, please clarify in the text."

- We did mean that the D2 and D4 dependence is removed from the entire Lagrangian. For clarification, we have
changed the sentence after equation (2.21) "which completely removes all D2 and D4 dependence from the Lagrangian." 

into 

"which removes the D2 and D4 dependence from Z_\phi. Since the Lagrangian in the physical
basis depended on D2 and D4 only through Z_\phi, this completely removes the redundant
couplings from the Lagrangian."

-----------------------------------------------------------------

Above equation (2.5), we changed "The allowed redefinition is" to "The allowed redefinition to dimension-six is"

-----------------------------------------------------------------

Below equation (2.6), we added a footnote after "renormalized fields" for extra clarification on the considered field redefinitions and rescalings.

-----------------------------------------------------------------

Below equation (2.9) we changed "and gives $Z_\tphi=Z_\phi$." into "and $Z_\tphi=Z_\phi$ with the rescaling choice Eq. (2.8)."

-----------------------------------------------------------------


